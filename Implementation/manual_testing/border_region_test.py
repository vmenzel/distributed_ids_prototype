# border_region_test.py
# This file is not meant as an exhaustive unit test/regression test, but as an examplary and helper file. 
# by Verena
# version 0.1

from virtual_grid.border_region import border_region
from virtual_grid.virtual_components.power_line import power_line
from virtual_grid.virtual_components.meter import meter
from virtual_grid.virtual_components.switch import switch


def main():
    #test_req()

    #update_test()

    load_json_test()


def test_req():

    global my_border_region
    my_border_region = border_region("B1")

    line1 = power_line("l1", 5, 400, 0)
    line2 = power_line("l2", 5, 400, 0)
    line3 = power_line("l3", 5, 400, 0)

    my_border_region.assign_power_line(line1)
    my_border_region.assign_power_line(line2)
    my_border_region.assign_power_line(line3)

    meter11 = meter("m11", [], line1, 50, 400)
    meter12 = meter("m12", [], line1, 50, 400)
    meter21 = meter("m21", [], line2, 50, 400)
    meter22 = meter("m22", [], line2, 50, 400)
    meter31 = meter("m31", [], line3, 50, 400)
    meter32 = meter("m32", [], line3, 50, 400)

    line1.attach_meter(meter11)
    line1.attach_meter(meter12)
    line2.attach_meter(meter21)
    line2.attach_meter(meter22)
    line3.attach_meter(meter31)
    line3.attach_meter(meter32)

    my_border_region.assign_meter(meter11)
    my_border_region.assign_meter(meter12)
    my_border_region.assign_meter(meter21)
    my_border_region.assign_meter(meter22)
    my_border_region.assign_meter(meter31)
    my_border_region.assign_meter(meter32)

    switch1 = switch("s1", [], line2)
    my_border_region.assign_switch(switch1)
    line2.attach_switch(switch1)

    meter11.update_current(5)
    meter12.update_current(5)
    meter21.update_current(10)
    meter22.update_current(10)
    meter31.update_current(15)
    meter32.update_current(5)

    meter11.update_voltage(15)
    meter12.update_voltage(10)
    meter21.update_voltage(15)
    meter22.update_voltage(15)
    meter31.update_voltage(10)
    meter32.update_voltage(10)

    switch1.update_state(0)

    my_border_region.check_neighbourhood_requirements()


def update_test():

    my_border_region.print_border_values()
    switches = [1]
    meter_current = [5, 10, 25, 6, 5, 4]
    voltage_current = [5, 8, 4, 2, 1, 5]

    my_border_region.update_values(switches, voltage_current, meter_current)

    my_border_region.print_border_values()


def load_json_test():
    my_border_region = border_region("b2")
    my_border_region.load_topology()


# Needed to execute file as main
if __name__ == '__main__':
    main()
