# subgrid_test.py
# This file is not meant as an exhaustive unit test/regression test, but as an examplary and helper file. 
# by Verena
# version 0.1

from virtual_grid.subgrid import subgrid
from virtual_grid.virtual_components.power_line import power_line
from virtual_grid.virtual_components.bus import bus
from virtual_grid.virtual_components.meter import meter
from virtual_grid.virtual_components.switch import switch


def main():
    #test_req()
    #update_test()
    load_json_test()


def test_req():
    global my_subgrid
    my_subgrid = subgrid("S1")

    # TC 1, 2 lines ( 1 in, 1 out), bleide gleicher Strom
    line1 = power_line("l1", 5, 400, 1)
    my_subgrid.assign_power_line(line1)

    line2 = power_line("l2", 5, 400, 0)
    my_subgrid.assign_power_line(line2)

    bus1 = bus("b1", line1, line2)
    my_subgrid.assign_bus(bus1)

    meter1 = meter("m1", bus1, line1, 10, 400)
    meter1.update_current(4)
    meter1.update_voltage(5)
    my_subgrid.assign_meter(meter1)

    meter2 = meter("m2", bus1, line2, 10, 400)
    meter2.update_current(4)
    my_subgrid.assign_meter(meter2)

    # TC 2, 3 lines (2in, 1out), in Summe unterschiedlich
    line3 = power_line("l3", 5, 400, 1)
    my_subgrid.assign_power_line(line3)

    bus2 = bus("b2", line2, line3)
    bus2.add_inc(line1)
    my_subgrid.assign_bus(bus2)

    meter3 = meter("m3", bus2, line3, 10, 400)
    meter3.update_current(5)
    my_subgrid.assign_meter(meter3)

    meter4 = meter("m4", bus2, line2, 3, 400)
    meter4.update_current(4)
    my_subgrid.assign_meter(meter4)

    meter5 = meter("m5", bus2, line1, 10, 1)
    meter5.update_current(2)
    line1.attach_meter(meter5)
    my_subgrid.assign_meter(meter5)

    # REQ 2
    meter5.update_voltage(2)

    # REQ 3
    switch1 = switch("s1", bus1, line1)
    line1.attach_switch(switch1)
    line1.attach_meter(meter1)
    my_subgrid.assign_switch(switch1)

    my_subgrid.check_local_requirements()

    my_subgrid.print_subgrid_values()


def update_test():

    switches = [1]

    meter_current = [5, 10, 25, 6, 3]

    voltage_current = [5, 8, 4, 2, 1]

    my_subgrid.update_values(switches, voltage_current, meter_current)

    my_subgrid.print_subgrid_values()


def load_json_test():

    my_subgrid = subgrid("S1")
    my_subgrid.load_topology()


# Needed to execute file as main
if __name__ == '__main__':
    main()