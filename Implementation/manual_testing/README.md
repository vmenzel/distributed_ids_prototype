# Manual testing    

This directory contains a few small helper files which *manually* tests the virtual subgrid without any large topology configuration or monitor managment. 

As this manual testing was not within the scope of this thesis, the files were not commented in exhaustive. Additionaly, the files do not argue to cover the complete functionality of all virtual regions. 

However, the files were kept to give insight on how manual test/Unittest could be structured, or how individual functions work. 