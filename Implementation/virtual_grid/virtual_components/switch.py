# switch.py
# by Verena
# version 0.1
'''
Switch class
Represents the virtual switch in the monitoring system
'''
import virtual_grid.virtual_components.power_line
import virtual_grid.virtual_components.bus

from virtual_grid.virtual_components.component import component


class switch(component):
    def __init__(self, name, bus, power_line):
        super().__init__(name)
        self.__assigned_bus = bus
        self.__assigned_power_line = power_line
        self.__state = 0
        pass

    def update_state(self, new_state):
        # sets the state of the meter to 'new_state'
        self.__state = new_state

    def get_state(self):
        # returns the current state of the meter
        return self.__state

    def get_assigned_bus(self):
        # returns the bus, that the meter is assigned to
        return self.__assigned_bus

    def get_assinged_power_line(self):
        # returns the power line that the meter is assigned to
        return self.__assigned_power_line
