# neighbourhood_monitor.py
# by Verena
# version 0.1
'''
Neighbourhood monitor class 
Represents the monitoring of the neighbourhood scope from an organisational POV. 
'''

from virtual_grid.border_region import border_region


class neighbourhood_monitor():
    def __init__(self, name, neighbourhood_name, topology):
        self.id = name

        self.__switch_count = 0
        self.__current_count = 0
        self.__voltage_count = 0

        self.__virtual_border_region = border_region(neighbourhood_name,
                                                     topology)
        self.__virtual_border_region.load_topology()

        self.__recieved_neighbour_update = False
        self.__recieved_local_update = False

        self.m_print("Creation successfull.")

    def recieve_secure_data_update(self, origin, switch_values, meter_values):
        # function that recieves the data from the local monitors
        # if data for one step is recieved from both relevant local monitors, the requirement check is started

        if origin == "N" and self.__recieved_neighbour_update == False:
            # Update from the local monitor located at the other field station

            self.__recieved_neighbour_update = True
            self.update_values(switch_values, meter_values)
            self.m_print(
                "Recieved Updates from the neighbouring field station")

            if self.__recieved_local_update:
                self.m_print(
                    "Recieved both parts of the information set.\n Starting to check the neighbourhood requirements."
                )
                self.__virtual_border_region.check_neighbourhood_requirements()

                self.__recieved_local_update = False
                self.__recieved_neighbour_update = False

        elif origin == "L" and self.__recieved_local_update == False:
            # update recieved from the local monitor located at the same field station

            self.__recieved_local_update = True
            self.update_values(switch_values, meter_values)
            self.m_print("Recieved Updates from the local field station")

            if self.__recieved_neighbour_update:
                self.m_print(
                    "Recieved both parts of the information set.\n Starting to check the neighbourhood requirements."
                )
                self.__virtual_border_region.check_neighbourhood_requirements()

                self.__recieved_local_update = False
                self.__recieved_neighbour_update = False
        else:
            self.m_print(
                "Error: Something went wrong. Somehow recieved data updates from the same local monitor two times in a row."
            )

    def update_values(self, switch_values, meter_values):
        # updates all values of switches and meters within the border region

        for switch in self.__virtual_border_region.get_all_switches():
            for switch_update in switch_values:
                if switch_update[0] == switch.get_name():
                    switch.update_state(switch_update[1])

        for meter in self.__virtual_border_region.get_all_meters():
            for meter_update in meter_values:
                if meter_update[0] == meter.get_name():
                    meter.update_voltage(meter_update[1])
                    meter.update_current(meter_update[2])


# HELPER FUNCTIONS

    def register_components(self):
        # returns an array with the names of relevant switches and meters for the border region
        switches = []
        for s in self.__virtual_border_region.get_all_switches():
            switches.append(s.get_name())

        meters = []
        for m in self.__virtual_border_region.get_all_meters():
            meters.append(m.get_name())

        return [switches, meters]

    def m_print(self, statement):
        # simple print function
        print("[" + self.id + "]: " + statement)
