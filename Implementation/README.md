# Implementation

This directory contains the monitoring system to supervise two local subgrids as described in my thesis. <br>
The monitoring system support to ways of input: 1) direct connection to the simulation in the *Testbed* directory or 2) (historically) generated csv files.<br>

## Overview

#### Top Level Files 

`monitor_managment.py` Main file of the monitoring systems. It starts the monitoring, creates and connects the concrete monitors with each other and with their input source.<br>
`local_monitor.py` Local monitor supervising a local, virtual subgrid. This does **not** evaluate the requirements directly but rather connects new data input sets with the virtual representation and manages the communication with the neighbourhood monitors.<br>
`neighbourhood_monitor.py` Just as the local_monitor.py does **not** directly check the local requirements, the neighbourhood monitor does not either. It rather sends new data input to the virtual representation of the matching border region and manages the communication with the complete monitoring system.<br>

#### Test input Files

This directory contains input files which are needed for the monitoring system to e.g. load the grid topology or read input data for the scenario types 1-4. Additionaly this directory contains the results of the scenario execution. 

#### Manual Testing 

Basic test classes to test the monitoring system or rather the virtual grids without the complete testbed. As the manual testing was not within the core scope of this thesis, the files included here are not exhaustive. 

#### Virtual Subgrid

Virtual representaion of the actual (simulated) grid to aid the monitoring approach. Here the concrete evaluation of desired requirements and components will be executed. 

## Installation

For the implementation is only one dependency needed: 

    $ pip install termcolor 

Further dependencies are not needed.     

## Start of the implementation

The implementation can easily be started with 

    $ python monitor_managment.py 

This start the monitoring with historical data from scenario type 1. 

**Caution**: If the terminal cannot display color via termcolor, the output of the requirement evaluation might be starting with `←[32m` and ending with `←[0m`. This is due to the color. I personally reccomend to use the power shell within Visual Studio Code. 

### Start a special scenario type: 

The input files for the different scenarios can be selected in LOC 30 and 32 of the `monitor_management.py`
   
    $ global input_file_0
    $ input_file_0 = "input_files/scenario_1_subgrid_0.csv"
    $ global input_file_1
    $ input_file_1 = "input_files/scenario_1_subgrid_1.csv"

Simply exchange the scenario number 1 to any number between 2-4. The matching scenario files then will be loaded when starting the monitor managment with: 

    $ python monitor_managment.py 

### Start the live connection to the testbed: 

First of all, the testbed needs to be started before the monitoring can be started. Further information on how to start the testbed can be found in the testbed folder. 

Additionaly, one manipulations needs to be done within the `monitor_managment.py` in LOC 26 from 1 to 0:
    
    $ global read_data_from_file
    $ read_data_from_file = 1  # 1 = uses specified files from the input_files folder, 0 = tries to connect "live" to the simulation

Then, the monitor_managment can be started normal with: 
    
    $ python monitor_managment.py 

and automatically connects to the testbed. 

### Turing "detailed print" option on/off

The implementation offers an option to either report the evaluation for each requirement back to the monitoring system or report for each component each requirement back to the monitoring system. 

This option can be set from "normal" to detailed print in the `virtual_grid\subgrid.py` in LOC 25:

    $ self.__detailed_print = 0  # 0 = uses the basic alert/print system , 1 = uses the detailed alert/print system

and in the `virtual_grid\border_region.py` LOC 21: 

    $ self.__detailed_print = 0  # 0 = uses the basic alert/print system , 1 = uses the detailed alert/print system