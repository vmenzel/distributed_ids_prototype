# monitor_managment.py
# by Verena
# version 0.1
'''
Monitor Managment
Programm that managnes the different monitors, their communciation with eachother and the communication with their input sources.
'''

# pymodbus3 was installed from chromik/pymodbus and not via pip install pymodbus
from pymodbus3.constants import Endian
from pymodbus3.payload import BinaryPayloadDecoder
from pymodbus3.payload import BinaryPayloadBuilder
from pymodbus3.client.sync import ModbusTcpClient as ModbusClient

from time import sleep
import os
from datetime import datetime

from local_monitor import local_monitor
from neighbourhood_monitor import neighbourhood_monitor

global local_monitor_1
global local_monitor_2

global read_data_from_file
read_data_from_file = 1  # 1 = uses specified files from the input_files folder, 0 = tries to connect "live" to the simulation

# configuration which scenario should be loaded
global input_file_0
input_file_0 = "input_files/scenario_1_subgrid_0.csv"
global input_file_1
input_file_1 = "input_files/scenario_1_subgrid_1.csv"

global RTU_1
RTU_1 = []
global RTU_2
RTU_2 = []


def main():
    # main function of the monitoring, creates, starts and closes the monitoring system

    print("Creation and initialization of monitors:")
    create_and_connect_monitors()

    print("----------------")
    print("Starting the monitoring...")
    monitoring()

    print("----------------")
    print("End of monitoring. Closing.")
    close_connections()
    pass


def create_and_connect_monitors():
    # function to initialize all monitors and connect them

    # Create monitors
    global local_monitor_1
    local_monitor_1 = local_monitor("Local Monitor 0", "Virtual Subgrid 0",
                                    "input_files/rtu_0.json", read_data_from_file)

    neighbourhood_monitor_1 = neighbourhood_monitor(
        "Neighbourhood Monitor 0", "Virtual Border Region 1",
        "input_files/border_region_0_1.json")

    global local_monitor_2
    local_monitor_2 = local_monitor("Local Monitor 1", "Virtual Subgrid 1",
                                    "input_files/rtu_1.json", read_data_from_file)

    neighbourhood_monitor_2 = neighbourhood_monitor(
        "Neighbourhood Monitor 1", "Virtual Border Region 1",
        "input_files/border_region_0_1.json")

    print("----------------")

    local_monitor_1.set_grid_information(
        1, 12, 12
    )  # count of switches, meters reporting voltage and meters reporting current
    local_monitor_2.set_grid_information(2, 12, 12)

    # connect local monitors to their input source
    if read_data_from_file:
        # Configuration t
        local_monitor_1.recieve_csv_input_info(input_file_0)
        local_monitor_2.recieve_csv_input_info(input_file_1)

        print("Starting monitoring from historical files.")
    else:
        local_monitor_1.connect(10502)
        local_monitor_2.connect(10503)
        print("Starting live monitoring.")
    print("----------------")

    # connect monitors to eachother
    local_monitor_1.connect_with_neighbourhood(neighbourhood_monitor_2,
                                               neighbourhood_monitor_1)
    local_monitor_2.connect_with_neighbourhood(neighbourhood_monitor_1,
                                               neighbourhood_monitor_2)

    print("Connected locals to significant other neighbourhood monitor.")


def monitoring():
    # function to do the actual monitoring

    for i in range(20):
        try:
            print("\nData set " + str(i))
            local_monitor_1.waitForData()
            local_monitor_2.waitForData()
        except KeyboardInterrupt:
            print("------------------------------------------")
            print("Monitor Management was terminated manually.")
            exit(0)


def close_connections():
    # function to close the connections to the inputs
    if not read_data_from_file:
        local_monitor_1.close_client_connection()
        local_monitor_2.close_client_connection()


# Needed to execute file as main
if __name__ == '__main__':
    main()
