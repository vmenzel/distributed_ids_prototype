# local_monitor.py
# by Verena
# version 0.1
'''
Local monitor class 
Represents the monitoring of the local scope from the organisational POV.
'''

# pymodbus3 was installed from chromik/pymodbus and not via pip 
from pymodbus3.constants import Endian
from pymodbus3.payload import BinaryPayloadDecoder
from pymodbus3.payload import BinaryPayloadBuilder
from pymodbus3.client.sync import ModbusTcpClient as ModbusClient
from pymodbus3.server.sync import ModbusTcpServer as ModbusServer

from datetime import datetime
from time import sleep
import os
import csv

from virtual_grid.subgrid import subgrid


class local_monitor():
    def __init__(self, name, subgrid_name, topology, read_input):
        self.id = name
        self.__neighbour = []
        self.__local_neighourhood_monitor = []
        self.__input_file_name = []
        self.__switch_count = 0
        self.__current_switch_values = []
        self.__current_count = 0
        self.__current_current_values = []
        self.__voltage_count = 0
        self.__current_voltage_values = []
        self.__border_meters = []
        self.__border_switches = []

        self.__read_data_from_file = read_input
        self.__input_rows_read = 1
        self.__virtual_subgrid = subgrid(subgrid_name, topology)
        self.__virtual_subgrid.load_topology()

        self.m_print("Creation successfull.")

    def connect_with_neighbourhood(self, neighbours, local_neighbourhood):
        # function used to connect the local monitor to their neighbours
        # in future version of course, not only one neighbour but a whole neighbourhood should be connected
        self.__neighbour = neighbours
        self.__local_neighourhood_monitor = local_neighbourhood

        self.recieve_border_components(self.__neighbour.register_components())
        # recieves the information from the neighbourhood monitor which components are important for the border region


    def waitForData(self):
        # function for monitoring, checks out new data sets and then checks the requirements on them
        # dependin on whether live data or an input file is used 
        if self.__read_data_from_file:
            self.read_from_file_input()
        else:
            self.decode_Data_from_live(self.__input_client, 1)

    def connect(self, ip_port):
        # connection to the live data source (the RTU)
        RTU = ModbusClient('127.0.0.1', port=ip_port)
        RTU.connect()

        self.__input_client = RTU
        self.m_print("Connected to Modbus Client.")
        pass


    def set_grid_information(self, switch_count, current_count, voltage_count):
    # sets the count of how many switches, voltage measured by meters and currents measured by meters are used for this local monitor
        self.__switch_count = switch_count
        self.__current_count = current_count
        self.__voltage_count = voltage_count

    def recieve_csv_input_info(self, filename):
    # sets the correct input path
        self.__input_file_name = filename

    def read_from_file_input(self):
        # reads a new row from the input file and start the monitoring on the newly recieved data

        with open(self.__input_file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=";")
            # to prevent reading the title row, the first row will be skipped
            for i in range(self.__input_rows_read):
                # previously read data will be skipped
                title = csv_reader.__next__()

            self.__input_rows_read = self.__input_rows_read + 1
            row = csv_reader.__next__()

            switch_updates = []
            voltage_updates = []
            current_updates = []

            for x in range(self.__switch_count):
                switch_updates.append(row[x])

            for y in range(self.__voltage_count):
                voltage_updates.append(row[self.__switch_count + y])

            for z in range(self.__current_count):
                current_updates.append(row[self.__switch_count +
                                           self.__voltage_count + z])

            self.__virtual_subgrid.update_values(switch_updates,
                                                 voltage_updates,
                                                 current_updates)

            # start the actual monitoring check                                     
            self.check_local_and_send_to_neighbourhood()

            csv_file.close()


    def decode_Data_from_live(self, client, count):
        # Function to read one value step read from the RTU and parse it readible into matching arrays and start the monitoring on the new data
        # similiar to the reading in the client_connection.py

        for row in range(count):
            # switch reading
            switches_result = self.__input_client.read_coils(
                0, self.__switch_count, unit=1)
            switch_updates = []
            for x in range(self.__switch_count):
                switch_updates.append(switches_result.bits[x])

            # voltage and current reading
            result = self.__input_client.read_holding_registers(
                0, self.__voltage_count * 8, unit=1)

            decoder = BinaryPayloadDecoder.from_registers(result.registers,
                                                          endian=Endian.Big)

            voltage_updates = []
            current_updates = []
            for x in range(self.__voltage_count * 2):
                if x < self.__voltage_count:
                    voltage_updates.append(decoder.decode_64bit_float())
                else:
                    current_updates.append(decoder.decode_64bit_float())

            self.__virtual_subgrid.update_values(switch_updates,
                                                 voltage_updates,
                                                 current_updates)

            # start the actual monitoring check     
            self.check_local_and_send_to_neighbourhood()


    def check_local_and_send_to_neighbourhood(self):
        # check data with scope a)
        # and send it to both relevant neighbourhood monitors
        self.__virtual_subgrid.check_local_requirements()
        
        self.send_current_data_to_local()
        self.send_current_data_to_neighbour()
        
        sleep(1)

    def recieve_border_components(self, components):
        # function to set the information about which components will be relevant for the border region 
        for switch in self.__virtual_subgrid.get_all_switches():
            for s in components[0]:
                if switch.get_name() == s:
                    self.__border_switches.append(switch)

        for meter in self.__virtual_subgrid.get_all_meters():
            for m in components[1]:
                if meter.get_name() == m:
                    self.__border_meters.append(meter)

    def send_current_data_to_neighbour(self):
        # function to send the data to the neighbourhood monitor available at the other subgrid 
        current_switches = []
        current_meter_values = []

        for switch in self.__border_switches:
            current_switches.append([switch.get_name(), switch.get_state()])
        for meter in self.__border_meters:
            current_meter_values.append(
                [meter.get_name(),
                 meter.get_voltage(),
                 meter.get_current()])

        self.__neighbour.recieve_secure_data_update("N", current_switches,
                                                    current_meter_values)

    def send_current_data_to_local(self):
        # function to send the data to the neighbourhood monitor which is available at the local grid
        current_switches = []
        current_meter_values = []

        for switch in self.__border_switches:
            current_switches.append([switch.get_name(), switch.get_state()])
        for meter in self.__border_meters:
            current_meter_values.append(
                [meter.get_name(),
                 meter.get_voltage(),
                 meter.get_current()])

        self.__local_neighourhood_monitor.recieve_secure_data_update(
            "L", current_switches, current_meter_values)
        sleep(1)


# HELPER FUNCTIONS

    def m_print(self, statement):
        # simple print function
        print("[" + self.id + "]: " + statement)

    def close_client_connection(self):
        # function to close the connection to the input client
        self.__input_client.close()

    def get_current_switch_values(self):
        # returns an array of all current switch values
        return self.__current_switch_values

    def get_current_current_values(self):
        # returns an array of all current current values
        return self.__current_current_values

    def get_current_voltage_values(self):
        # returns an array of all current voltage values
        return self.__current_voltage_values
