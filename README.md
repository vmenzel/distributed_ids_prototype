# Securing SCADA networks for smart grids via adistributed evaluation of local sensor data

This repository is a proof of concept work to investigate the possibilities of the usage of local information to help raise the overall security in SCADA networks via a hieracichal, process-aware monitoring approach. 

# Overview / Abstract
Within smart grids, the safe and dependable distribution of electric power highly depends on the security of Supervisory Control and Data Acquisition (SCADA) systems and their underlying communication protocols. 
Existing network-based intrusion detection systems for Industrial Control Systems (ICS) are usually centrally applied at the SCADA server and do not take the underlying physical process into account. 
A recent line of work proposes an additional layer of security via a process-aware approach applied locally at the field stations. 
This paper broadens the scope of process-aware monitoring by considering the interaction between neighboring field stations, which facilitates upcoming trends of decentralized energy management (DEM). 
Local security monitoring is lifted to monitoring neighborhoods of field stations, therefore achieving a broader grid coverage w.r.t. security.
We provide a distributed monitoring algorithm of the generated sensory readings for this extended setting.
The feasibility of the approach is shown via a prototype simulation testbed and a scenario with two subgrids, which are available in this repository.

## Directory Overview 

**Testbed**<br>
Here you can find a co-simulation based on the Mosaik framework. It consists of a small dutch neighbourhood and two RTUs who propagate the data generated from the simulation to their local substation managment systems. 
<br>

**Implementation**<br>
Here you can find a monitoring tool that takes the data generated from the simulation and checks it during different evaluation loops against both physical laws and security requirements to ensure a safe operation of the system. 
<br>


# Installation instructions 

Installation instructions can be found in both directories seperatly. The implementation can be started with the scenario input files directly. For the live connection, please start the testbed in another terminal first. 